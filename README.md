# The simple Snake game created with C++ and SFML library

## Building with Linux

1) Intall SFML library
```sh
sudo apt install libsfml-dev
```

2) Use chmod command to be able to run Shell scripts.
```sh
chmod u+x *.sh
```

3) Run scripts in order and the game will show
```sh
./configure.sh
./rebuild.sh
./run.sh
```

:)
